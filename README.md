
# Orchid #

Orchid is a minimal logic language implemented in C.
It can be used to check simple proofs and confirm sound reasoning.



## How to compile it ##

To build Orchid you'll need `make` and a C compiler (the makefile used `gcc` by default).
Installing is as simple as running `sudo make install`.



## How to use it ##

Once installed you can run Orchid programs like so: `orchid [PROGRAM FILE]`.
A sample program, called 'example.orch', is provided for testing.



## About the language ##

Syntatically the language is fairly basic.
A program consists of a sequence of statements.
Statements are arrangements of expressions.


An expression can be an Atom, Variable, or List.

An Atom is a fixed value used to represent a specific object or property.
Atoms are written as sequences of letters beginning with a lowercase.
For example:

```
cat

fish

red

```


A Variable is a placeholder value used to generically represent an Atom or List.
Variables are written as sequences of letters beginning with an uppercase.
For example:

```
X

People

TheThing

```


A List is a sequence of expressions.
Lists are written as zero or more expressions seperated by whitespace between '(' and ')'.
For example:

```
()

(carrot parsley)

((one fish) (two fish) (red fish) (blue fish))

(X is bigger than Y)

```


A statement can be an Axiom, Rule, or Query.

An Axiom is merely an expression stated to be true.
Axioms are terminated by a '.' character.
For example:

```
(apple is a fruit).

(cats have fur).

(father is the parent of son).

```


A Rule is a sequence of conditional expressions which result in an implication.
Rules are also terminated by a '.' character.
The conditions of a Rule are seperated by ',' characters.
For example:

```
(X walks) => (X has legs).

(PersonA loves PersonB) => (PersonB loves PersonA).

(It is furry), (It has a tail) => (It is a cat).

```


A Query is an expression which the user wishes to prove.
Queries are terminated by a '?' character.
For example:

```
(cat is furry)?

(programmers write code)?

(person has Something)?

```


After parsing the program, the interpreter goes through all the queries and tries to prove them.
The interpreter outputs the result of its attempt to prove each query. 



## How it works ##

Orhcid uses a simple unification engine to check if expressions are equivalent.
By recursively attempting to prove sub-conditions it is able to reason about simple relationships.
The interpreter makes no attempt to hinder recursive implications.
Unfortunantly though, this means it is possible to write non terminating programs.



## TODO ##

Orchid is still missing lots of things.
In the future some features which may be added or improved are:

- More data types
- Better error messages
- Refactoring parser code
- Checks of program completeness
- An interactive REPL

