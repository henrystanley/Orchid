
# Grammar #

The following is a Backus-Naur form specification of the language grammar:


```

<prog> ::= <statement> | <statement> <ws> <prog>
<ws> ::= "" | <ws-char> <ws>
<ws-char> ::= "\n" | "\t" | " " | "\r"
<statement> ::= <axiom> | <rule> | <query>
<axiom> ::= <spaced-expr> "."
<rule> ::= <rule-conds> "=>" <spaced-expr> "."
<rule-conds> ::= <spaced-expr> | <spaced-expr> "," <rule-conds>
<query> ::= <spaced-expr> "?"
<spaced-expr> ::= <ws> <expr> <ws>
<expr> ::= <atom> | <var> | <list>
<atom> ::= <lowercase> | <lowercase> <letters>
<var> ::= <uppercase> | <uppercase> <letters>
<lowercase> ::= "a" | "b" | "c" | ... | "z"
<uppercase> ::= "A" | "B" | "C" | ... | "Z"
<letters> ::= <letter> | <letter> <letters>
<letter> ::= <lowercase> | <uppercase>
<list> ::= "(" <list-tail>
<list-tail> ::= <ws> ")" | <spaced-expr> <list-tail>

```
