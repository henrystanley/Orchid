CC=gcc
CFLAGS=-c -Wall -g -std=c11
EXECUTABLE=orchid

SRC=$(wildcard ./src/*.c)
OBJ=$(subst ./src,./obj,$(patsubst %.c,%.o,$(wildcard ./src/*.c)))

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJ)
	mkdir -p bin
	$(CC) $(OBJ) -o bin/$(EXECUTABLE)

obj/%.o: src/%.c
	mkdir -p obj
	$(CC) $(CFLAGS) -o $@ $^

clean:
	rm -rf ./obj ./bin

run: $(EXECUTABLE)
	./bin/$(EXECUTABLE)

valgrind: $(EXECUTABLE)
	valgrind --leak-check=full --show-reachable=yes --track-origins=yes --log-fd=1 bin/$(EXECUTABLE)

install: $(EXECUTABLE)
	cp bin/$(EXECUTABLE) /usr/bin