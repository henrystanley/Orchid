
#include "query.h"



////////////////////////
/// Query Resolution ///
////////////////////////

// Attempts to prove a query given a program containing rules and axioms
int prove(Program *prog, Expr *expr, Bind **binds_expr) {
  ProgramLink *p = prog->first;
  while (p) {

    // Returns true if a matching axiom is found
    if (p->statement->type == AXIOM) {
      Bind *fresh_binds = new_Bind();
      Bind *binds_expr_copy = copy_Bind(*binds_expr);
      if (unify(&binds_expr_copy, expr, &fresh_binds, p->statement->val.expr)) {
        free_Bind(*binds_expr);
        free_Bind(fresh_binds);
        *binds_expr = binds_expr_copy;
        return 1;
      }
      free_Bind(fresh_binds);
      free_Bind(binds_expr_copy);
    }
    
    // If a matching rule is found tries to prove the necessary conditions
    if (p->statement->type == RULE) {
      Rule *rule = p->statement->val.rule;
      Bind *fresh_binds = new_Bind();
      Bind *binds_expr_copy = copy_Bind(*binds_expr);
      if (unify(&binds_expr_copy, expr, &fresh_binds, rule->implication)) {
        ExprListLink *cond = rule->conditions->first;
        int sub_prove = 1;
        while (cond && sub_prove) {
          sub_prove &= prove(prog, cond->expr, &fresh_binds);
          cond = cond->next;
        }
        if (sub_prove) {
          free_Bind(*binds_expr);
          free_Bind(fresh_binds);
          *binds_expr = binds_expr_copy;
          return 1;
        }
      }
      free_Bind(fresh_binds);
      free_Bind(binds_expr_copy);
    }

    p = p->next;
  }
  // Returns false by default
  return 0;
}



///////////////////
/// Unification ///
///////////////////

/*

Unification is the process used to determine if two symbolic expressions are
equivalent. If variables are present it identifies which values they should
be bound to so as to make the expressions match (if possible).

An atom may unify with another atom if they are equal.
An unbound variable may unify with any term, and will be bound to that term.
A bound variable may unify only with a term which unifies with its bound value.
A list may bind with another list where the terms of the second list unify with
the terms of the first.

*/

// Checks if two arbitary expressions unify
int unify(Bind **binds_a, Expr *a, Bind **binds_b, Expr *b) {

  // fail if either expression is NULL
  if (!a || !b) return 0;

  // succeed if two of the same atoms are unified
  if (a->type == ATOM && b->type == ATOM && a->val.sym_id == b->val.sym_id)
    return 1;

  // succeed if the contents of two lists unify
  if (a->type == LIST && b->type == LIST) {
    ExprList *a_list = a->val.list;
    ExprList *b_list = b->val.list;
    ExprListLink *a_link = a_list->first;
    ExprListLink *b_link = b_list->first;
    while (a_link && b_link) {
      if (!unify(binds_a, a_link->expr, binds_b, b_link->expr)) return 0;
      a_link = a_link->next;
      b_link = b_link->next;
    }
    if (!a_link && !b_link) return 1;
    else return 0;
  }
  
  
  // succeed if a is unbound and bind b to a
  // otherwise try to unify b and the bound expression of a
  // if that bound expression happens to be a variable simply check equality
  if (a->type == VAR) {
    Expr *a_bind = get_var(*binds_a, a->val.sym_id);
    if (a_bind) {
      if (a_bind->type == VAR && b->type == VAR)
        return a_bind->val.sym_id == b->val.sym_id;
      else return unify(binds_a, a_bind, binds_b, b);
    } else {
      bind_var(binds_a, a->val.sym_id, b);
      return 1;
    }
  }

  // succeed if b is unbound and bind a to b
  // otherwise try to unify a and the bound expression of b
  if (b->type == VAR) {
    Expr *b_bind = get_var(*binds_b, b->val.sym_id);
    if (b_bind) return unify(binds_a, a, binds_b, b_bind);
    else {
      bind_var(binds_b, b->val.sym_id, a);
      return 1;
    }
  }


  // fail by default
  return 0;

}
