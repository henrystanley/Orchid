#ifndef LEXER_HEADER
#define LEXER_HEADER



#include <stdlib.h>
#include <stdio.h>




/////////////
/// Types ///
/////////////


// A LexState holds the state of a given src file being lexed
// All lexing functions take a LexState as input
typedef struct {
  int pos, line_num, column_num;
  char *src;
} LexState;

// A LexResult is simply the product of lexing, i.e. a token and a new LexState
// A LexResult also stores if the lex was successful or not
// All lexing functions return a LexResult as output
typedef struct {
  int success;
  char *token;
  LexState state;
} LexResult;



/////////////////
/// Functions ///
/////////////////

LexState new_LexState(char *src);

LexResult lex_left_paren(LexState state);

LexResult lex_right_paren(LexState state);

LexResult lex_question_mark(LexState state);

LexResult lex_period(LexState state);

LexResult lex_null_char(LexState state);

LexResult lex_comma(LexState state);

LexResult lex_implies(LexState state);

LexResult lex_atom(LexState state);

LexResult lex_var(LexState state);



#endif
