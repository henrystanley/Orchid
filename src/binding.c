
#include "binding.h"



////////////////
/// Bindings ///
////////////////

// Returns a pointer to a new bind stack
Bind *new_Bind() {
  return NULL;
}

// Deallocates a bind stack
void free_Bind(Bind *bind) {
  Bind *tmp;
  while (bind) {
    tmp = bind->next;
    free(bind);
    bind = tmp;
  }
}

// Copies a bind stack
Bind *copy_Bind(Bind *bind) {
  Bind *copy = new_Bind();
  while (bind) {
    bind_var(&copy, bind->sym_id, bind->expr);
    bind = bind->next;
  }
  return copy;
}

// Prints a bind stack
void print_Bind(SymTable *symtable, Bind *binds) {
  while (binds) {
    printf("%s <- ", lookup_symbol(symtable, binds->sym_id));
    print_Expr(symtable, binds->expr);
    printf("\n");
    binds = binds->next;
  }
}

// Binds a sym_id and an expression within a bind stack
void bind_var(Bind **binds, int sym_id, Expr *expr) {
  Bind *new_bind = malloc(sizeof(Bind));
  new_bind->sym_id = sym_id;
  new_bind->expr = expr;
  new_bind->next = *binds;
  *binds = new_bind;
}

// Retreives an expression given a sym_id from a bind stack
// Returns NULL if no binding can be found
Expr *get_var(Bind *binds, int sym_id) {
  while (binds) {
    if (binds->sym_id == sym_id) return binds->expr;
    binds = binds->next;
  }
  return NULL;
}
