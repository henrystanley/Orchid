#ifndef BINDING_HEADER
#define BINDING_HEADER



#include "ast.h"
#include "symtable.h"



/////////////
/// Types ///
/////////////

// Binding is represented as a linked list of associated sym_ids and exprs
// Obviously lookup is O(n), which is somewhat rubbish
// In the future this data structure should be replaced by a hash table
typedef struct {
  int sym_id;
  Expr *expr;
  struct Bind *next;
} Bind;



/////////////////
/// Functions ///
/////////////////

Bind *new_Bind();

void free_Bind(Bind *bind);

Bind *copy_Bind(Bind *bind);

void print_Bind(SymTable *symtable, Bind *binds);

void bind_var(Bind **binds, int sym_id, Expr *expr);

Expr *get_var(Bind *binds, int sym_id);



#endif