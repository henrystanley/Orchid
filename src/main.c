
#include <stdio.h>
#include <stdlib.h>
#include "run.h"

int main(int argc, char** argv) {
  if (argc == 2) {
    char *program = slurp_file(argv[1]);
    run(program);
  } else printf("Usage: orchid [FILE]\n");
  return 0;
}
