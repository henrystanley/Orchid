#ifndef PARSER_HEADER
#define PARSER_HEADER



#include <stdlib.h>
#include "lexer.h"
#include "ast.h"
#include "symtable.h"



/////////////
/// Types ///
/////////////

// A ParseResult is used as a return value from parser functions
// It holds the parse status, resulting LexState, and parsed value
typedef struct {
  int success;
  LexState lexstate;
  void *parsed_val;
} ParseResult;





/////////////////
/// Functions ///
/////////////////

ParseResult parse_Expr(SymTable *symtable, LexState lexstate);

ParseResult parse_Atom(SymTable *symtable, LexState lexstate);

ParseResult parse_Var(SymTable *symtable, LexState lexstate);

ParseResult parse_List(SymTable *symtable, LexState lexstate);

ParseResult parse_Statement(SymTable *symtable, LexState lexstate);

ParseResult parse_Axiom(SymTable *symtable, LexState lexstate);

ParseResult parse_Rule(SymTable *symtable, LexState lexstate);

ParseResult parse_Query(SymTable *symtable, LexState lexstate);

ParseResult parse_Program(SymTable *symtable, LexState lexstate);



#endif
