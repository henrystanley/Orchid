
#include "run.h"


// Read an entire file into memory
char *slurp_file(char *filename) {
  FILE *f = fopen(filename, "r");
  int buffer_size = DEFAULT_SLURP_BUFFER_SIZE;
  int buffer_pos = 0;
  char *buffer = malloc(buffer_size);
  char c = getc(f);
  while (c != EOF) {
    if (buffer_pos >= buffer_size) {
      buffer_size *= 2;
      buffer = realloc(buffer, buffer_size);
    }
    buffer[buffer_pos++] = c;
    c = getc(f);
  }
  if (buffer_pos >= buffer_size)
    buffer = realloc(buffer, buffer_size+1);
  buffer[buffer_pos] = '\0';
  fclose(f);
  return buffer;
}


// Tries to parse and run a program
// returns 1 on success and 0 on failure
int run(char *raw_prog) {

  // Init
  SymTable *symtable = new_SymTable();
  LexState lexstate = new_LexState(raw_prog);

  // Parsing
  ParseResult parse = parse_Program(symtable, lexstate);
  if (!parse.success) {
    printf("ERROR: Parse Error due to invalid syntax\n");
    free_SymTable(symtable);
    return 0;
  }
  Program *prog = parse.parsed_val;

  // Evaluating Queries
  ProgramLink *p = prog->first;
  while (p) {
    if (p->statement->type == QUERY) {
      Bind *empty_binds = new_Bind();
      int query_true = prove(prog, p->statement->val.expr, &empty_binds);
      free_Bind(empty_binds);
      print_Statement(symtable, p->statement);
      printf("  =  %s\n\n", query_true ? "TRUE" : "FALSE");
    }
    p = p->next;
  }

  // Cleanup
  free_Program(prog);
  free_SymTable(symtable);
  return 1;
}