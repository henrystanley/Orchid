#ifndef AST_HEADER
#define AST_HEADER



#include <stdlib.h>
#include <stdio.h>
#include "symtable.h"



/////////////
/// Types ///
/////////////

// Expressions can be Atoms, Variables, or Lists
typedef enum {
  ATOM,
  VAR,
  LIST
} ExprType;

// Exprs hold their type and either a symbol identifier or a pointer to a list
typedef struct {
  ExprType type;
  union {
    int sym_id;
    struct ExprList *list;
  } val;
} Expr;

// A linked list of Exprs

typedef struct ExprListLink {
  Expr *expr;
  struct ExprListLink *next;
} ExprListLink;

typedef struct {
  ExprListLink *first;
  ExprListLink *last;
} ExprList;

// Statements can be Axioms, Rules, or Queries
typedef enum {
  AXIOM,
  RULE,
  QUERY
} StatementType;

// Statements hold their type and either an Expr or pointer to a rule
typedef struct {
  StatementType type;
  union {
    Expr *expr;
    struct Rule *rule;
  } val;
} Statement;

// A Rule consists of conditions and an implication
typedef struct Rule {
  ExprList *conditions;
  Expr *implication;
} Rule;

// A program is a linked list of statements

typedef struct ProgramLink {
  Statement *statement;
  struct ProgramLink *next;
} ProgramLink;

typedef struct Program {
  ProgramLink *first;
  ProgramLink *last;
} Program;


/////////////////
/// Functions ///
/////////////////

Expr *new_Atom(int sym_id);

Expr *new_Var(int sym_id);

Expr *new_List();

void free_Expr(Expr *expr);

void print_Expr(SymTable *symtable, Expr *expr);

ExprList *new_ExprList();

void append_to_end_of_ExprList(ExprList *list, Expr *expr);

void free_ExprList(ExprList *list);

void print_ExprList(SymTable *symtable, ExprList *list);

Statement *new_Axiom(Expr *expr);

Statement *new_Rule(ExprList *conditions, Expr *implication);

Statement *new_Query(Expr *expr);

void free_Statement(Statement *statement);

void print_Statement(SymTable *symtable, Statement *statement);

Program *new_Program();

void free_Program(Program *program);

void append_to_end_of_Program(Program *program, Statement *statement);

void print_Program(SymTable *symtable, Program *program);



#endif
