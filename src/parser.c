
#include "parser.h"



///////////////
/// Parsers ///
///////////////

// Attempts to parse a single expression (an Atom, Var, or List)
ParseResult parse_Expr(SymTable *symtable, LexState lexstate) {
  ParseResult sub;

  // Atom
  sub = parse_Atom(symtable, lexstate);
  if (sub.success == 1) return sub;

  // Var
  sub = parse_Var(symtable, lexstate);
  if (sub.success == 1) return sub;

  // List
  sub = parse_List(symtable, lexstate);
  if (sub.success == 1) return sub;

  // Failure state
  sub.success = 0;
  sub.lexstate = lexstate;
  sub.parsed_val = NULL;
  return sub;
}

// Attempts to parse an Atom expression
ParseResult parse_Atom(SymTable *symtable, LexState lexstate) {
  LexResult lex = lex_atom(lexstate);
  ParseResult result;
  result.lexstate = lex.state;
  if (lex.success) {
    result.success = 1;
    result.parsed_val = new_Atom(append_symbol(symtable, lex.token));
  } else {
    result.success = 0;
    result.parsed_val = NULL;
    free(lex.token);
  }
  return result;
}

// Attempts to parse a Var expression
ParseResult parse_Var(SymTable *symtable, LexState lexstate) {
  LexResult lex = lex_var(lexstate);
  ParseResult result;
  result.lexstate = lex.state;
  if (lex.success) {
    result.success = 1;
    result.parsed_val = new_Var(append_symbol(symtable, lex.token));
  } else {
    result.success = 0;
    result.parsed_val = NULL;
    free(lex.token);
  }
  return result;
}

// Attempts to parse a List expression
ParseResult parse_List(SymTable *symtable, LexState lexstate) {
  LexResult lex = lex_left_paren(lexstate);
  ParseResult result;
  if (lex.success) {
    Expr *list = new_List();
    result = parse_Expr(symtable, lex.state);
    while (result.success) {
      append_to_end_of_ExprList(list->val.list, (Expr *)result.parsed_val);
      result = parse_Expr(symtable, result.lexstate);
    }
    lex = lex_right_paren(result.lexstate);
    result.lexstate = lex.state;
    if (lex.success) {
      result.success = 1;
      result.parsed_val = list;
    } else {
      result.success = 0;
      result.parsed_val = NULL;
      free_Expr(list);
    }
  } else {
    result.success = 0;
    result.parsed_val = NULL;
  }
  return result;
}

// Attempts to parse a statement
ParseResult parse_Statement(SymTable *symtable, LexState lexstate) {
  ParseResult result;

  // Axiom
  result = parse_Axiom(symtable, lexstate);
  if (result.success == 1) return result;

  // Rule
  result = parse_Rule(symtable, lexstate);
  if (result.success == 1) return result;

  // Query
  result = parse_Query(symtable, lexstate);
  if (result.success == 1) return result;

  // Failure state
  result.success = 0;
  result.lexstate = lexstate;
  result.parsed_val = NULL;
  return result;
}

ParseResult parse_Axiom(SymTable *symtable, LexState lexstate) {
  ParseResult result;

  // Espression
  ParseResult expr = parse_Expr(symtable, lexstate);
  if (expr.success) {
    // Period
    LexResult period = lex_period(expr.lexstate);
    if (period.success) {
      // Success
      result.success = 1;
      result.lexstate = period.state;
      result.parsed_val = new_Axiom(expr.parsed_val);
      return result;
    } else { // Failure Period
      free_Expr(expr.parsed_val);
    }
  }

  // Failure state
  result.success = 0;
  result.lexstate = lexstate;
  result.parsed_val = NULL;
  return result;
}

ParseResult parse_RuleConditions(SymTable *symtable, LexState lexstate) {
  LexState lex = lexstate;
  ParseResult result;
  ExprList *conds = new_ExprList();
  
  // Condition expressions
  for (;;) {
    result = parse_Expr(symtable, lex);
    if (!result.success) { // Failure state
      free_ExprList(conds);
      result.success = 0;
      result.lexstate = lexstate;
      result.parsed_val = NULL;
      return result;
    }
    append_to_end_of_ExprList(conds, (Expr *)result.parsed_val);
    LexResult lex_c = lex_comma(result.lexstate);
    if (!lex_c.success) { lex = result.lexstate; break; }
    lex = lex_c.state;
  }

  // Success
  result.success = 1;
  result.lexstate = lex;
  result.parsed_val = conds;
  return result;
}

ParseResult parse_Rule(SymTable *symtable, LexState lexstate) {
  ParseResult result;

  // Conditions
  ParseResult conditions = parse_RuleConditions(symtable, lexstate);
  if (!conditions.success) goto fail_conditions;

  // Implies
  LexResult implies = lex_implies(conditions.lexstate);
  if (!implies.success) goto fail_implies;

  // Implication
  ParseResult implication = parse_Expr(symtable, implies.state);
  if (!implication.success) goto fail_implication;

  // Period
  LexResult period = lex_period(implication.lexstate);
  if (!period.success) goto fail_period;

  // Success
  result.success = 1;
  result.lexstate = period.state;
  result.parsed_val = new_Rule(conditions.parsed_val, implication.parsed_val);
  return result;
  
  // Failure
  fail_period:
    free_Expr(implication.parsed_val);
  fail_implication:
  fail_implies:
    free_ExprList(conditions.parsed_val);
  fail_conditions:
    result.success = 0;
    result.lexstate = lexstate;
    result.parsed_val = NULL;
    return result;
}

ParseResult parse_Query(SymTable *symtable, LexState lexstate) {
    ParseResult result;

    // Expression
    ParseResult expr = parse_Expr(symtable, lexstate);
    if (expr.success) {
      // Question mark
      LexResult question_mark = lex_question_mark(expr.lexstate);
      if (question_mark.success) {
        // Success
        result.success = 1;
        result.lexstate = question_mark.state;
        result.parsed_val = new_Query(expr.parsed_val);
        return result;
      } else { // Failure Question mark
        free_Expr(expr.parsed_val);
      }
    }

    // Failure state
    result.success = 0;
    result.lexstate = lexstate;
    result.parsed_val = NULL;
    return result;
  }


ParseResult parse_Program(SymTable *symtable, LexState lexstate) {
  ParseResult result;

  Program *program = new_Program();
  ParseResult sub;
  for (;;) {
    sub = parse_Statement(symtable, lexstate);
    if (sub.success) {
      append_to_end_of_Program(program, sub.parsed_val);
      lexstate = sub.lexstate;
    }
    else {
      if (lex_null_char(lexstate).success) break;
      else {
        // Failure State
        free_Program(program);
        result.success = 0;
        result.lexstate = lexstate;
        result.parsed_val = NULL;
        return result;
      }
    }
  }

  // Success
  result.success = 1;
  result.lexstate = lexstate;
  result.parsed_val = program;
  return result;
}
