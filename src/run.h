#ifndef RUN_HEADER
#define RUN_HEADER


#include "stdio.h"
#include "stdlib.h"
#include "lexer.h"
#include "parser.h"
#include "symtable.h"
#include "query.h"


//////////////////
/// Parameters ///
//////////////////

#define DEFAULT_SLURP_BUFFER_SIZE 2048



/////////////////
/// Functions ///
/////////////////

char *slurp_file(char *filename);

int run(char *raw_prog);


#endif