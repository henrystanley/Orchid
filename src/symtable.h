#ifndef SYMTABLE_HEADER
#define SYMTABLE_HEADER



#include <stdlib.h>
#include <string.h>



//////////////////
/// Parameters ///
//////////////////

// Starting capacity of a freshly allcoated SymTable
#define SYMTABLE_STARTING_CAPACITY 20



/////////////
/// Types ///
/////////////

// A SymTable is a dynamic list which associates a string with an integer
// Symbols can be appended in O(n) time and looked up in O(1) time
// Lukily, appending is performed rather sparingly (only in parsing)
typedef struct {
  char **symbols;
  int length, capacity;
} SymTable;



/////////////////
/// Functions ///
/////////////////

SymTable *new_SymTable();

void free_SymTable(SymTable *symtable);

int append_symbol(SymTable *symtable, char *symbol);

char *lookup_symbol(SymTable *symtable, int sym_id);



#endif
