
#include "ast.h"



/////////////
/// Exprs ///
/////////////

// Allocates and returns a new Atom expression
Expr *new_Atom(int sym_id) {
  Expr *expr = malloc(sizeof(Expr));
  expr->type = ATOM;
  expr->val.sym_id = sym_id;
  return expr;
}

// Allocates and returns a new Variable expression
Expr *new_Var(int sym_id) {
  Expr *expr = malloc(sizeof(Expr));
  expr->type = VAR;
  expr->val.sym_id = sym_id;
  return expr;
}

// Allocates and returns a new empty List expression
Expr *new_List() {
  Expr *expr = malloc(sizeof(Expr));
  expr->type = LIST;
  expr->val.list = new_ExprList();
  return expr;
}

// Deallocates an expression
void free_Expr(Expr *expr) {
  if (expr->type == LIST) free_ExprList(expr->val.list);
  free(expr);
}

// Prints an expression
void print_Expr(SymTable *symtable, Expr *expr) {
  if (expr->type == LIST) {
    printf("(");
    print_ExprList(symtable, expr->val.list);
    printf(")");
  } else printf("%s", lookup_symbol(symtable, expr->val.sym_id));
}



/////////////////
/// ExprLists ///
/////////////////

// Returns a pointer to a new ExprList
ExprList *new_ExprList() {
  ExprList *list = malloc(sizeof(ExprList));
  list->first = NULL;
  list->last = NULL;
  return list;
}

// Appends a new expression to the end of an ExprList
void append_to_end_of_ExprList(ExprList *list, Expr *expr) {
  ExprListLink *new_link = malloc(sizeof(ExprListLink));
  new_link->expr = expr;
  new_link->next = NULL;
  if (list->last) list->last->next = new_link;
  else list->first = new_link;
  list->last = new_link;
}

// Deallocates an ExprList
void free_ExprList(ExprList *list) {
  ExprListLink *l = list->first;
  while (l) {
    free_Expr(l->expr);
    ExprListLink *tmp = l->next;
    free(l);
    l = tmp;
  }
  free(list);
}

// Prints an ExprList
void print_ExprList(SymTable *symtable, ExprList *list) {
  ExprListLink *link = list->first;
  while (link) {
    print_Expr(symtable, link->expr);
    if (link->next) printf(" ");
    link = link->next;
  }
}



//////////////////
/// Statements ///
//////////////////

// Allocates and returns a new Axiom statement
Statement *new_Axiom(Expr *expr) {
  Statement *statement = malloc(sizeof(statement));
  statement->type = AXIOM;
  statement->val.expr = expr;
  return statement;
}

// Allocates and returns a new Rule statement
Statement *new_Rule(ExprList *conditions, Expr *implication) {
  Statement *statement = malloc(sizeof(statement));
  statement->type = RULE;
  Rule *rule = malloc(sizeof(Rule));
  rule->conditions = conditions;
  rule->implication = implication;
  statement->val.rule = rule;
  return statement;
}

// Allocates and returns a new Query statement
Statement *new_Query(Expr *expr) {
  Statement *statement = malloc(sizeof(statement));
  statement->type = QUERY;
  statement->val.expr = expr;
  return statement;
}

// Deallocates a statement
void free_Statement(Statement *statement) {
  if (statement->type == RULE) {
    free_ExprList(statement->val.rule->conditions);
    free_Expr(statement->val.rule->implication);
    free(statement->val.rule);
  } else free_Expr(statement->val.expr);
  free(statement);
}

// Prints a statement
void print_Statement(SymTable *symtable, Statement *statement) {
  ExprListLink *link;
  switch (statement->type) {
    case AXIOM:
      print_Expr(symtable, statement->val.expr);
      printf(".");
      break;
    case RULE:
      link = statement->val.rule->conditions->first;
      while (link->next) {
        print_Expr(symtable, link->expr);
        printf(", ");
        link = link->next;
      }
      print_Expr(symtable, link->expr);
      printf(" => ");
      print_Expr(symtable, statement->val.rule->implication);
      printf(".");
      break;
    case QUERY:
      print_Expr(symtable, statement->val.expr);
      printf("?");
      break;
  }
}


////////////////
/// Programs ///
////////////////

// Returns a pointer to a new Program
Program *new_Program() {
  Program *program = malloc(sizeof(Program));
  program->first = NULL;
  program->last = NULL;
  return program;
}

// Deallocates a Program
void free_Program(Program *program) {
  ProgramLink *s = program->first;
  while (s) {
    free_Statement(s->statement);
    ProgramLink *tmp = s->next;
    free(s);
    s = tmp;
  }
  free(program);
}

// Appends a new statement to the end of a Program
void append_to_end_of_Program(Program *program, Statement *statement) {
  ProgramLink *new_link = malloc(sizeof(ProgramLink));
  new_link->statement = statement;
  new_link->next = NULL;
  if (program->last) program->last->next = new_link;
  else program->first = new_link;
  program->last = new_link;
}

// Prints a Program
void print_Program(SymTable *symtable, Program *program) {
  ProgramLink *s = program->first;
  while (s) {
    print_Statement(symtable, s->statement);
    if (s->next) printf("\n");
    s = s->next;
  }
}
