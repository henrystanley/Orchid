
#include "symtable.h"



/////////////////////
/// Symbol Tables ///
/////////////////////

// Allocates and returns a new SymTable
SymTable *new_SymTable() {
  SymTable *symtable = malloc(sizeof(SymTable));
  symtable->symbols = malloc(SYMTABLE_STARTING_CAPACITY * sizeof(char *));
  symtable->length = 0;
  symtable->capacity = SYMTABLE_STARTING_CAPACITY;
  return symtable;
}

// Deallocates a SymTable
void free_SymTable(SymTable *symtable) {
  for (int i = 0; i < symtable->length; i++) free(symtable->symbols[i]);
  free(symtable->symbols);
  free(symtable);
}

// Adds a new symbol to a SymTable and returns the symbol id
int append_symbol(SymTable *symtable, char *symbol) {
  // First we look to see if the symbol is already in the table
  for (int i = 0; i < symtable->length; i++) {
    if (strcmp(symbol, symtable->symbols[i]) == 0) {
      free(symbol);
      return i;
    }
  }
  // Next we check to see if we need to allocate more space for the table
  if (symtable->length == symtable->capacity) {
    // If so, we allocate a new array twice the size and move all the symbols
    symtable->capacity = symtable->capacity*2;
    char **new_symbols = malloc(symtable->capacity * sizeof(char *));
    for (int i = 0; i < symtable->length; i++) {
      new_symbols[i] = symtable->symbols[i];
    }
    free(symtable->symbols);
    symtable->symbols = new_symbols;
  }
  // Finally, we append our new symbol
  symtable->symbols[symtable->length] = symbol;
  symtable->length++;
  return (symtable->length - 1);
}

// Lookup a symbol from a symtable by its sym_id
// If no such sym_id is in the table, returns NULL
char *lookup_symbol(SymTable *symtable, int sym_id) {
  if (sym_id < 0 || sym_id >= symtable->length) return NULL;
  return symtable->symbols[sym_id];
}
