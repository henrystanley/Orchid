
#include "lexer.h"



/////////////////////////
/// Character Classes ///
/////////////////////////

// Checks if a given character is a whitespace character
int is_whitespace(char c) {
  return ((c == ' ') || (c == '\n') || (c == '\t') || (c == '\r'));
}

// Checks if a given character is a lowercase letter
int is_lowercase(char c) {
  return ((c > 96) && (c < 123));
}

// Checks if a given character is an uppercase letter
int is_uppercase(char c) {
  return ((c > 64) && (c < 91));
}

// Checks if a given character is a letter
int is_letter(char c) {
  return (is_uppercase(c) || is_lowercase(c));
}

// Checks if a given character is a digit
int is_digit(char c) {
  return ((c > 47) && (c < 58));
}



////////////////////
/// Lexing Utils ///
////////////////////

// Initializes a new LexState
LexState new_LexState(char *src) {
  LexState state;
  state.pos = 0;
  state.line_num = 0;
  state.column_num = 0;
  state.src = src;
  return state;
}

// Reads the current character from a LexState
char get_char(LexState state) {
  return state.src[state.pos];
}

// Increments pos, line_num, and column_num
LexState advance(LexState state) {
  char c = state.src[state.pos];
  if (c == '\0') return state;
  state.pos++;
  state.column_num++;
  if (c == '\n') {
    state.line_num++;
    state.column_num = 0;
  }
  return state;
}

// Advances the position of a LexState until the current char is not whitespace
LexState skip_whitespace(LexState state) {
  while (is_whitespace(get_char(state))) state = advance(state);
  return state;
}



//////////////
/// Lexers ///
//////////////

// Tries to lex a chunk of letters from a LexState
LexResult lex_word(LexState state) {
  state = skip_whitespace(state);
  int success = 0;
  int word_capacity = 8;
  int word_len = 0;
  char *word = malloc(word_capacity * sizeof(char));
  char c = get_char(state);
  while (is_letter(c)) {
    if (word_len >= word_capacity) {
      word_capacity *= 2;
      word = realloc(word, word_capacity);
    }
    word[word_len++] = c;
    state = advance(state);
    c = get_char(state);
  }
  if (word_len >= word_capacity) word = realloc(word, word_capacity+1);
  word[word_len] = '\0';
  if (word_len != 0) success = 1;
  return (LexResult){ success, word, state };
}

// Tries to lex a single character
LexResult lex_char(LexState state, char c) {
  state = skip_whitespace(state);
  int success = 0;
  char h = get_char(state);
  if (h == c) success = 1;
  char token[2]; token[0] = h; token[1] = '\n';
  return (LexResult){ success, token, advance(state) };
}

LexResult lex_left_paren(LexState state) { return lex_char(state, '('); }

LexResult lex_right_paren(LexState state) { return lex_char(state, ')'); }

LexResult lex_question_mark(LexState state) { return lex_char(state, '?'); }

LexResult lex_period(LexState state) { return lex_char(state, '.'); }

LexResult lex_null_char(LexState state) { return lex_char(state, '\0'); }

LexResult lex_comma(LexState state) { return lex_char(state, ','); }

LexResult lex_implies(LexState state) {
  state = skip_whitespace(state);
  if (get_char(state) == '=') {
    state = advance(state);
    if (get_char(state) == '>') {
      state = advance(state);
      return (LexResult){ 1, "=>", state };
    }
  }
  return (LexResult){ 0, "=>", state };
}

LexResult lex_atom(LexState state) {
  LexResult result = lex_word(state);
  if (!result.success) return result;
  if (!is_lowercase(result.token[0])) result.success = 0;
  return result;
}

LexResult lex_var(LexState state) {
  LexResult result = lex_word(state);
  if (!result.success) return result;
  if (!is_uppercase(result.token[0])) result.success = 0;
  return result;
}
