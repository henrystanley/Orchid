#ifndef QUERY_HEADER
#define QUERY_HEADER



#include "binding.h"
#include "ast.h"



/////////////////
/// Functions ///
/////////////////

int prove(Program *prog, Expr *expr, Bind **binds_expr);

int unify(Bind **binds_a, Expr *a, Bind **binds_b, Expr *b);



#endif